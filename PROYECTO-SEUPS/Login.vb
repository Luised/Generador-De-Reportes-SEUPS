﻿Imports System.Data.SqlClient
Imports System.IO

Public Class Login
    Private Sub BACEPTAR_Click(sender As Object, e As EventArgs) Handles BACEPTAR.Click
        Conexion()
    End Sub
    Private Sub BSALIR_Click(sender As Object, e As EventArgs) Handles BSALIR.Click
        If MessageBox.Show("Desea Salir?", "Salir", MessageBoxButtons.OKCancel, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) = DialogResult.OK Then
            End 'Se sale del programa
        End If
    End Sub

    Private Sub Login_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        ' TXTuser.Text = "sa"
        ' TXTpass.Text = "sistemas"
    End Sub

    Private Sub TXTuser_KeyPress(sender As Object, e As KeyPressEventArgs) Handles TXTuser.KeyPress
        Letra(TXTuser, 20, e)
    End Sub

    Private Sub TXTpass_KeyPress(sender As Object, e As KeyPressEventArgs) Handles TXTpass.KeyPress
        LetraYNumero(TXTpass, 20, e)
    End Sub

    Private Sub Conexion()
        Try
            Usuario = TXTuser.Text
            Contrasena = TXTpass.Text
            Using cnn As New SqlConnection("Data Source=" & ServerName & "; Initial Catalog=master; User id=" & Usuario & "; password=" & Contrasena & ";")
                Dim iincert As New SqlCommand
                cnn.Open()
                iincert.Connection = cnn

                iincert.CommandText = "if db_id('SEUPS') is null begin CREATE DATABASE SEUPS end;"
                iincert.ExecuteNonQuery()

                cnn.Close()
            End Using
            Using cn As New SqlConnection("Data Source=" & ServerName & "; Initial Catalog=SEUPS; User id=" & Usuario & "; password=" & Contrasena & ";")
                Dim iincert As New SqlCommand
                cn.Open()
                iincert.Connection = cn
                'creacion de las tablas...
                iincert.CommandText = "if object_id('BATERIAS' , 'U') is null begin create table BATERIAS(ID_BAT Varchar (8) primary key not null,MODE_BAT Varchar (15) not null,SERIE_BAT Varchar (20) not null,MARCA_BAT Varchar (20) not null,CAPACIDAD_BAT Varchar (30) not null,CANTI_BAT Varchar (80) not null) end;"
                iincert.ExecuteNonQuery() 'baterias

                iincert.CommandText = "if object_id('SUCURSALES' , 'U') is null begin create table SUCURSALES(ID_SUC Varchar (8) primary key not null,CALLE_SUC Varchar (50) not null,NUM_SUC Varchar (5) not null,CIU_SUC Varchar (20) not null,ESTADO_SUC Varchar (10) not null,NOM_EMP_SUC Varchar (30) not null,NOM_SUC Varchar (30) not null,TEL_SUC Varchar (15) not null,NOM_GEREN_SUC Varchar (30) not null,TEL_GEREN_SUC Varchar (15) not null,NOM_SUBGEREN_SUC Varchar (30) not null,TEL_SUBGEREN_SUC Varchar (15) not null,) end;"
                iincert.ExecuteNonQuery() 'sucursales

                iincert.CommandText = "if object_id('UPS' , 'U') is null begin create table UPS(ID_UPS Varchar (8)primary key not null,ID_SUC Varchar (8) not null,SERIE_UPS Varchar (20) not null,MARCA_UPS Varchar (20) not null,MOD_UPS Varchar (15) not null,ID_BAT Varchar (8),foreign key (ID_BAT) references BATERIAS(ID_BAT),foreign key (ID_SUC) references SUCURSALES(ID_SUC)) end;"
                iincert.ExecuteNonQuery() 'ups

                iincert.CommandText = "if object_id('MANTENIMIENTO' , 'U') is null begin create table MANTENIMIENTO(FOLIO_MANT Varchar (15) primary key not null,DIA_MANT Varchar (2) not null,MES_MANT Varchar (9) not null,ANIO_MANT Varchar (4) not null,HORA_MANT Varchar (8) not null,ID_SUC Varchar (8) not null,ID_UPS Varchar (8) not null,IMG_MANT Varchar (MAX) not null,foreign key (ID_SUC) references SUCURSALES(ID_SUC),foreign key (ID_UPS) references UPS(ID_UPS)) end;"
                iincert.ExecuteNonQuery() 'mantenimiento

                iincert.CommandText = "if object_id('PROGRAMA' , 'U') is null begin create table PROGRAMA(ID_PRO Varchar (8) primary key not null,DIA_PRO Varchar (2) not null,MES_PRO Varchar (9) not null,ANIO_PRO Varchar (4) not null,HORA_PRO Varchar (8) not null,ID_SUC Varchar (8) not null,foreign key (ID_SUC) references SUCURSALES(ID_SUC)) end;"
                iincert.ExecuteNonQuery() 'programa

                iincert.CommandText = "if object_id('Setings' , 'U') is null begin create table Setings(FolderOutput Varchar (max)) insert into Setings values('') end;"
                iincert.ExecuteNonQuery() 'setings

                cn.Close()
            End Using

            Try
                Using cnn As New SqlConnection("Data Source=" & ServerName & "; Initial Catalog=SEUPS; User id=" & Usuario & "; password=" & Contrasena & ";")
                    Dim consulta As String = "Select * from Setings"
                    Dim cmd As New SqlCommand(consulta, cnn)
                    cmd.CommandType = CommandType.Text
                    Dim da As New SqlDataAdapter(cmd)
                    Dim dt As New DataTable
                    da.Fill(dt)
                    cnn.Open()
                    FolderOutput = dt.Rows(0).Item("FolderOutput")
                    cnn.Close()
                End Using
                If (FolderOutput.Length = 0) Then
                    FBD.Description = "Seleccione la carpeta que cotendra los archivos pdf."
                    If FBD.ShowDialog() = 1 Then
                        FolderOutput = FBD.SelectedPath
                        MsgBox(FolderOutput)
                        FolderOutput = Replace(FolderOutput, vbCrLf, "")
                        'guarda en el registro....
                        Using cnnn As New SqlConnection("Data Source=" & ServerName & "; Initial Catalog=SEUPS; User id=" & Usuario & "; password=" & Contrasena & ";")
                            Dim iincert As New SqlCommand
                            cnnn.Open()
                            iincert.Connection = cnnn

                            iincert.CommandText = "update Setings set FolderOutput='" & FolderOutput & "'"
                            iincert.ExecuteNonQuery()

                            cnnn.Close()
                        End Using

                    End If
                End If
                Dim hide_file_info As IO.FileInfo = My.Computer.FileSystem.GetFileInfo(FolderOutput)
                hide_file_info.Attributes = hide_file_info.Attributes Or IO.FileAttributes.Hidden

            Catch ex As Exception
                MsgBox("Error al leer lacarpeta..")
            End Try

            Me.Hide()
            MenuPrincipal.Show()
        Catch ex As Exception
            MsgBox("Usuario y/o Contraseña Incorrectas.")
        End Try
    End Sub

    Private Sub TXTuser_KeyDown(sender As Object, e As KeyEventArgs) Handles TXTuser.KeyDown
        If e.KeyCode = Keys.Enter Then
            Conexion()
        End If
    End Sub

    Private Sub TXTpass_KeyDown(sender As Object, e As KeyEventArgs) Handles TXTpass.KeyDown
        If e.KeyCode = Keys.Enter Then
            Conexion()
        End If
    End Sub
End Class