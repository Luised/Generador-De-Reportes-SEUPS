﻿Imports System.IO
Imports System.Data.SqlClient

Public Class MenuPrincipal
    Private Sub BateriasToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles BateriasToolStripMenuItem.Click
        Me.Enabled = False
        Baterias.Show()
    End Sub

    Private Sub MenuPrincipal_FormClosed(sender As Object, e As FormClosedEventArgs) Handles MyBase.FormClosed
        Try
            End 'Se sale del programa
        Catch ex As Exception
            MsgBox(ex)
        End Try
    End Sub
    Private Sub ProgramaToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ProgramaToolStripMenuItem.Click
        Me.Enabled = False
        Programa.Show()
    End Sub

    Private Sub RespaldosToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ToolStripMenuItem21.Click
        Me.Enabled = False
        Respaldos.Show()
    End Sub
    Private Sub SalirToolStripMenuItem1_Click(sender As Object, e As EventArgs) Handles ToolStripMenuItem12.Click
        If MessageBox.Show("Desea Salir?", "Salir", MessageBoxButtons.OKCancel, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) = DialogResult.OK Then
            Try
                End 'Se sale del programa
            Catch ex As Exception
                MsgBox(ex)
            End Try
        End If
    End Sub
    Private Sub UsuariosToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ToolStripMenuItem20.Click
        Me.Enabled = False
        Usuarios.Show()
    End Sub
    Private Sub SucursalesToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles SucursalesToolStripMenuItem.Click
        Me.Enabled = False
        Sucursales.Show()
    End Sub

    Private Sub UPSToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles UPSToolStripMenuItem.Click
        Me.Enabled = False
        UPSs.Show()
    End Sub
    Private Sub ReporteParaEmpresaToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ReporteParaEmpresaToolStripMenuItem.Click
        Me.Enabled = False
        ReporteEmpresa.Show()
    End Sub

    Private Sub ReporteParaClienteToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ReporteParaClienteToolStripMenuItem.Click
        Me.Enabled = False
        ReporteCliente.Show()
    End Sub

    Private Sub BorrarBaseDeDatosToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ToolStripMenuItem22.Click
        Me.Enabled = False
        ReiniciarDB.Show()
    End Sub

    Private Sub MantenimientosToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles MantenimientosToolStripMenuItem.Click
        Me.Enabled = False
        Mantenimientos.Show()
    End Sub

    Private Sub CambiarCarpetaDondeSeAlmacenanArchivosToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles CambiarCarpetaDondeSeAlmacenanArchivosToolStripMenuItem.Click
        Try
            FBD.Description = "Seleccione la carpeta que cotendra los archivos pdf."
            If FBD.ShowDialog() = 1 Then
                FolderOutput = FBD.SelectedPath
                MsgBox(FolderOutput)
                FolderOutput = Replace(FolderOutput, vbCrLf, "")
                'guarda en el registro....
                Using cnnn As New SqlConnection("Data Source=" & ServerName & "; Initial Catalog=SEUPS; User id=" & Usuario & "; password=" & Contrasena & ";")
                    Dim iincert As New SqlCommand
                    cnnn.Open()
                    iincert.Connection = cnnn

                    iincert.CommandText = "update Setings set FolderOutput='" & FolderOutput & "'"
                    iincert.ExecuteNonQuery()

                    cnnn.Close()
                End Using

            End If
            FO.Text = "Direccion de Almacenamiento de reportes: " & FolderOutput
            Dim hide_file_info As IO.FileInfo = My.Computer.FileSystem.GetFileInfo(FolderOutput)
            'hide_file_info.IsReadOnly = True
            hide_file_info.Attributes = hide_file_info.Attributes Or IO.FileAttributes.Hidden

        Catch ex As Exception
            MsgBox("Error al cambiar la direccion..")
        End Try
    End Sub

    Private Sub MenuPrincipal_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Using cnn As New SqlConnection("Data Source=" & ServerName & "; Initial Catalog=SEUPS; User id=" & Usuario & "; password=" & Contrasena & ";")
            Dim consulta As String = "Select * from Setings"
            Dim cmd As New SqlCommand(consulta, cnn)
            cmd.CommandType = CommandType.Text
            Dim da As New SqlDataAdapter(cmd)
            Dim dt As New DataTable
            da.Fill(dt)
            cnn.Open()
            FO.Text = "Direccion de Almacenamiento de reportes: " & dt.Rows(0).Item("FolderOutput")
            cnn.Close()
        End Using
    End Sub
End Class