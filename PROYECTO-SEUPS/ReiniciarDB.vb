﻿Imports System.Data.SqlClient
Public Class ReiniciarDB
    Private Sub Reiniciar_Click(sender As Object, e As EventArgs) Handles Reiniciar.Click
        If MessageBox.Show("Desea Reiniciar la base de datos? Ten en cuenta que se perderan todos los datos." & vbCrLf & "Y se eliminaran los reportes.", "Reiniciar", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1) = DialogResult.OK Then
            Try
                Using cn As New SqlConnection("Data Source=" & ServerName & "; Initial Catalog=SEUPS; User id=" & Usuario & "; password=" & Contrasena & ";")
                    Dim iincert As New SqlCommand
                    cn.Open()
                    iincert.Connection = cn

                    iincert.CommandText = "DELETE FROM MANTENIMIENTO"
                    iincert.ExecuteNonQuery() 'mantenimiento
                    iincert.CommandText = "DELETE FROM PROGRAMA"
                    iincert.ExecuteNonQuery() 'programa
                    iincert.CommandText = "DELETE FROM UPS"
                    iincert.ExecuteNonQuery() 'ups
                    iincert.CommandText = "DELETE FROM SUCURSALES"
                    iincert.ExecuteNonQuery() 'sucursales
                    iincert.CommandText = "DELETE FROM BATERIAS"
                    iincert.ExecuteNonQuery() 'baterias

                    Dim rutas = Dir(Replace(FolderOutput, vbCrLf, "") & "\*.pdf")
                    Do While rutas <> ""
                        Dim doc = FolderOutput & "\" & rutas
                        'MsgBox("Eliminando: " & doc)
                        Try
                            My.Computer.FileSystem.DeleteFile(doc, Microsoft.VisualBasic.FileIO.UIOption.AllDialogs, Microsoft.VisualBasic.FileIO.RecycleOption.SendToRecycleBin)
                        Catch ex As Exception
                            MsgBox("Error eliminando: " & doc)
                        End Try
                        rutas = Dir()
                    Loop

                    cn.Close()
                End Using
            Catch ex As Exception
                MsgBox("Error al reiniciar la base de datos.")
            End Try
            MenuPrincipal.Enabled = True
            Me.Close()
        End If
    End Sub

    Private Sub Cancelar_Click(sender As Object, e As EventArgs) Handles Cancelar.Click
        MenuPrincipal.Enabled = True
        Me.Close()
    End Sub

    Private Sub ReiniciarDB_FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        MenuPrincipal.Enabled = True
    End Sub
End Class