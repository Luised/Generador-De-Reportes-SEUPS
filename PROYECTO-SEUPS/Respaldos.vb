﻿Imports System.Data.SqlClient
Public Class Respaldos
    Private Sub Respaldos_FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        MenuPrincipal.Enabled = True
    End Sub
    Private Sub Respaldar_Click(sender As Object, e As EventArgs) Handles Respaldar.Click
        Regresar.Visible = False
        PBR.Visible = True
        PBR.Value = 0
        Dim SFD As New SaveFileDialog()
        SFD.Filter = "BAK Files (*.bak*)|*.bak"
        If SFD.ShowDialog = Windows.Forms.DialogResult.OK _
        Then
            Try
                Dim fm As String = SFD.FileName()
                Using cnnn As New SqlConnection("Data Source=" & ServerName & "; Initial Catalog=master; User id=" & Usuario & "; password=" & Contrasena & ";")
                    Dim iincert As New SqlCommand
                    cnnn.Open()
                    iincert.Connection = cnnn

                    iincert.CommandText = "backup database SEUPS to disk = '" & fm & "'"
                    iincert.ExecuteNonQuery()

                    cnnn.Close()
                End Using
                MsgBox("Base de Datos Respaldada Exitosamente...")
            Catch ex As Exception
                MsgBox("Error al respaldar la base de datos")
            End Try
        End If
        Regresar.Visible = True
        PBR.Visible = False
        PBR.Value = 0
    End Sub
    Private Sub Restaurar_Click(sender As Object, e As EventArgs) Handles Restaurar.Click
        Regresar.Visible = False
        PBR.Visible = True
        PBR.Value = 0
        Dim OFD As New OpenFileDialog()
        OFD.Filter = "BAK Files (*.bak*)|*.bak"
        If OFD.ShowDialog = Windows.Forms.DialogResult.OK _
        Then
            Try
                PBR.Value = 10
                Dim fm As String = OFD.FileName()
                Using cnnn As New SqlConnection("Data Source=" & ServerName & "; Initial Catalog=master; User id=" & Usuario & "; password=" & Contrasena & ";")
                    Dim iincert As New SqlCommand
                    cnnn.Open()
                    iincert.Connection = cnnn

                    iincert.CommandText = "restore database SEUPS from disk = '" & fm & "'"
                    iincert.ExecuteNonQuery()

                    cnnn.Close()
                End Using
                MsgBox("Base de Datos Restaurada Exitosamente...")
            Catch ex As Exception
                MsgBox(ex.Message)
            End Try
        End If
        Regresar.Visible = True
        PBR.Visible = False
        PBR.Value = 0
    End Sub

    Private Sub Respaldos_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Regresar.Visible = True
        PBR.Visible = False
        PBR.Value = 0
    End Sub

    Private Sub Regresar_Click(sender As Object, e As EventArgs) Handles Regresar.Click
        Me.Close()
    End Sub
End Class